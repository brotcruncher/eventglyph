#pragma once
#include <string>
#include <fstream>
#include "../libraries/glm/glm.hpp"

///
/// File format header:
/// 0..3 char* MagicIdentifier
/// 4..27 6x float (32 bit) Data set bounding box
/// 28..51 6x float (32 bit) Data set clipping box
/// 52..59 uint64_t Number of events
/// 60..63 float Maximum time of all events
///
/// File format body (relative bytes):
/// 0..11 3x float (32bit) Event position
/// 12..15 float Event time
/// 16..19 EventType (int) Event type
///


namespace eventglyph
{
	class mmse_file
	{
		public:

			mmse_file();
			~mmse_file();
			void attach_source(std::string path);
			void read_events(void* destination);
		
			#pragma pack(push, 1)
			struct header
			{
				char magic_identifyier[4];
				float bounding_box[6];
				float clipping_box[6];
				unsigned long long event_count;
				float maximum_time;
			};
			#pragma pack(pop)

			#pragma pack(push, 1)
			struct event
			{
				glm::vec3 position;
				float time;
				int type;
			};
			#pragma pack(pop)

			const header& file_header;

		private:

			header const_header;
			std::ifstream source_file;
	};
}