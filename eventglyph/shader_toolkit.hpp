#pragma once
#include <string>
#include "GL\glew.h"

namespace eventglyph
{
	using namespace std;

	class shader_program
	{
		public:
		
		shader_program();
		void attach(GLenum type, string path);
		void link();
		void use();
	
		private:

		GLuint id;
	};
	
	class shader_storage
	{
		public:

		shader_storage();
		void write_from(GLuint size, GLvoid *data);
		void read_to(GLuint size, GLvoid *data);
		void bind(GLuint position);

		private:

		GLuint id;
	};
}