#pragma once
#include "mmse_file.hpp"

namespace eventglyph
{
	#define horizontal_steps 15u
	#define vertical_steps 5u
	#define depth_steps 8u
	#define event_types 4u
	#define time_steps 150u

	#pragma pack(push, 1)
	struct camera
	{
		glm::vec3 position;
		glm::vec3 direction;
		glm::mat4 view;
		glm::mat4 projection;
		glm::uvec2 resolution;
	};
	#pragma pack(pop)


	struct tile_histogram
	{
		unsigned int histogram[event_types][depth_steps];
	};

	struct precision_time
	{
		float steps[event_types * depth_steps * time_steps];
	};

	struct polynom_time
	{
		glm::vec4 polynom[event_types * depth_steps];
	};

	struct range 
	{
		float minimum;
		float maximum;
	};

	enum time_mode
	{
		precision,
		polynom
	};

	class event_processor
	{
		public:

			event_processor();
			~event_processor();
			void attach_source(mmse_file& source);
			void project_events(camera& view);
			tile_histogram* grid;
			float* time;
			time_mode mode;

		private:

			mmse_file* source;
			mmse_file::event* events;
			int* events_tile;
			glm::vec4* events_projection;
			range* tiles_range;
			precision_time* tiles_precision_time;
			polynom_time* tiles_polynom_time;
	};
}
