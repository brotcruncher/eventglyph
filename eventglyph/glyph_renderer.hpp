#pragma once
#include "event_processor.hpp"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "shader_toolkit.hpp"

namespace eventglyph
{
	void glfw_error_callback(int error, const char* description);
	void framebuffer_size_callback(GLFWwindow* window, int width, int height);

	class glyph_renderer
	{
		public:
		
		glyph_renderer();
		~glyph_renderer();
		void attach_source(event_processor& histograms);
		void update_glyphs();
		void attach_events(mmse_file& events);
		void draw_glyphs();
		void draw_events();
		void draw_raster();
		void camera_update(camera&);
		void clear_buffer();	
		void swap_buffer();
		void draw_timeline();
		
		private:

		tile_histogram* source;
		float** time;
		time_mode* mode;
		GLFWwindow* window;
		GLuint vao;

		shader_program* event_shader;
		shader_program* glyph_shader;
		shader_program* raster_shader;
		shader_program* piechart_shader;
		shader_program* timeline_shader;

		shader_storage* events;
		shader_storage* vertices;
		shader_storage* indices;
		shader_storage* times;
		shader_storage* times_mode;
		shader_storage* matrices;
		shader_storage* raster;
		shader_storage* piecharts;
		shader_storage* timestep;

		unsigned int face_count;
		glm::vec2* vertex_buffer;
		glm::uvec3* index_buffer;
		
		#pragma pack(push, 1) 
		struct piechart
		{
			float distribution[event_types];
		};
		#pragma pack(pop)

		piechart* piechart_buffer;

		glm::dvec2 mouse;

		bool raster_active;
		bool events_active;
		bool glyphs_active;
		bool timeline_active;

		bool R_pressed;
		bool E_pressed;
		bool G_pressed;
		bool T_pressed;
		bool X_pressed;

		float ui_alpha;
		glm::uint time_step;
	};
}