#include "mmse_file.hpp"
#include <iostream>

namespace eventglyph
{
	mmse_file::mmse_file() : file_header(const_header)
	{
	}

	mmse_file::~mmse_file()
	{
		if (source_file.is_open()) source_file.close();
	}

	void mmse_file::attach_source(std::string path)
	{
		source_file.open(path, std::ios::binary | std::ios::beg);
		source_file.read((char*)&const_header, 64);
	}

	void mmse_file::read_events(void* destination)
	{
		source_file.seekg(64);
		source_file.read((char*)destination, sizeof(event) * file_header.event_count);
	}
}