﻿#include "glyph_renderer.hpp"
#include "../libraries/glm/gtx/rotate_vector.hpp"
#include <iostream>

namespace eventglyph
{
	void glfw_error_callback(int error, const char* description)
	{
		std::cout << "[particle_renderer] glfw error: " << description << std::endl;
		//exit(EXIT_FAILURE);
	}

	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		glViewport(0, 0, width, height);
	}

	glyph_renderer::glyph_renderer()
	{
		glfwSetErrorCallback(glfw_error_callback);

		if (!glfwInit()) exit(EXIT_FAILURE);

		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		window = glfwCreateWindow(100, 100, "Event Glyph", NULL, NULL);

		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
		{
			std::cout << "[glyph_renderer] glew initialization error." << std::endl;
			exit(EXIT_FAILURE);
		}

		std::cout << "[glyph_renderer] rendering context set up." << std::endl;

		event_shader = new shader_program;
		glyph_shader = new shader_program;
		raster_shader = new shader_program;
		piechart_shader = new shader_program;
		timeline_shader = new shader_program;

		events = new shader_storage;
		vertices = new shader_storage;
		indices = new shader_storage;
		times = new shader_storage;
		times_mode = new shader_storage;
		matrices = new shader_storage;
		raster = new shader_storage;
		piecharts = new shader_storage;
		timestep = new shader_storage;

		event_shader->attach(GL_VERTEX_SHADER, "../eventglyph/render_events.glvs");
		event_shader->attach(GL_FRAGMENT_SHADER, "../eventglyph/render_events.glfs");
		event_shader->link();

		raster_shader->attach(GL_VERTEX_SHADER, "../eventglyph/render_raster.glvs");
		raster_shader->attach(GL_FRAGMENT_SHADER, "../eventglyph/render_raster.glfs");
		raster_shader->link();

		glyph_shader->attach(GL_VERTEX_SHADER, "../eventglyph/render_glyphs.glvs");
		glyph_shader->attach(GL_FRAGMENT_SHADER, "../eventglyph/render_glyphs.glfs");
		glyph_shader->link();

		piechart_shader->attach(GL_VERTEX_SHADER, "../eventglyph/render_piecharts.glvs");
		piechart_shader->attach(GL_FRAGMENT_SHADER, "../eventglyph/render_piecharts.glfs");
		piechart_shader->link();

		timeline_shader->attach(GL_VERTEX_SHADER, "../eventglyph/render_timeline.glvs");
		timeline_shader->attach(GL_FRAGMENT_SHADER, "../eventglyph/render_timeline.glfs");
		timeline_shader->link();

		glGenVertexArrays(1, &vao);

		glm::uvec2 raster_conf(horizontal_steps, vertical_steps);
		raster->write_from(sizeof(unsigned int) * 2, &raster_conf);

		raster_active = true;
		events_active = true;
		glyphs_active = true;
		timeline_active = true;

		R_pressed = false;
		E_pressed = false;
		G_pressed = false;
		T_pressed = false;
		X_pressed = false;

		ui_alpha = 1.f;
		time_step = 0;
	}

	glyph_renderer::~glyph_renderer()
	{
	}

	void glyph_renderer::attach_source(event_processor& source)
	{
		this->source = source.grid;
		this->time = &source.time;
		this->mode = &source.mode;

		const unsigned int vertices_per_glyph = depth_steps + depth_steps * event_types;
		const unsigned int glyph_count = horizontal_steps * vertical_steps;
		unsigned int faces_per_layer = (depth_steps - 1) * 2;
		unsigned int faces_per_glyph = faces_per_layer * event_types;

		vertex_buffer = (glm::vec2*)calloc(vertices_per_glyph * glyph_count, sizeof(glm::vec2));
		index_buffer = (glm::uvec3*)calloc(faces_per_glyph, sizeof(glm::uvec3));
		piechart_buffer = (piechart*)calloc(glyph_count, sizeof(piechart));

		face_count = glyph_count * faces_per_glyph;

		for (unsigned int layer = 0; layer < event_types; layer++)
		{
			for (unsigned int face = 0; face < faces_per_layer / 2; face++)
			{	
				{
					glm::uvec3& current_face = index_buffer[layer * faces_per_layer + face * 2];

					current_face.x = layer * depth_steps + face;
					current_face.y = current_face.x + depth_steps;
					current_face.z = current_face.x + 1;
				}
				{
					glm::uvec3& current_face = index_buffer[layer * faces_per_layer + face * 2 + 1];

					current_face.x = layer * depth_steps + face + 1;
					current_face.y = current_face.x + depth_steps - 1;
					current_face.z = current_face.x + depth_steps;
				}
			}
		}
		
		indices->write_from(sizeof(glm::uvec3) * faces_per_glyph, index_buffer);
	}

	void glyph_renderer::update_glyphs()
	{	
		const unsigned int vertices_per_glyph = depth_steps + depth_steps * event_types;
		const unsigned int glyph_count = horizontal_steps * vertical_steps;

		float horizontal_step = 2.f / horizontal_steps;
		float vertical_step = 2.f / vertical_steps;

		for(unsigned int y = 0; y < vertical_steps; y++)
		{
			for(unsigned int x = 0; x < horizontal_steps; x++)
			{
				glm::vec2 central_vertex = glm::vec2(x * horizontal_step, y * vertical_step) - glm::vec2(1, 1) + glm::vec2(horizontal_step / 2.f, vertical_step / 2.f);
				unsigned int first_vertex = (y * horizontal_steps + x) * (vertices_per_glyph);

				// for each glyph read grid & rotate 360 / depth_steps degrees

				for (unsigned int depth = 0; depth < depth_steps; depth++)
				{
					vertex_buffer[first_vertex + depth] = glm::vec2(0, 0);
				}

				unsigned int sum[depth_steps] = { 0 };

				for(int type = 0; type < event_types; type++)
				{
					for(unsigned int depth = 0; depth < depth_steps; depth++)
					{
						sum[depth] += source[y * horizontal_steps + x].histogram[event_types - 1 - type][depth];

						vertex_buffer[first_vertex + depth_steps + type * depth_steps + depth] = glm::vec2(0, 1) * float(sum[depth]);
					}
				}

				unsigned int max = 0;

				for(unsigned int i = 0; i < depth_steps; i++)
				{
					if (sum[i] > max) max = sum[i];
				}

				for (unsigned int layer = 0; layer < event_types + 1; layer++)
				{
					for (unsigned int depth = 0; depth < depth_steps; depth++)
					{
						if (max != 0.f) vertex_buffer[first_vertex + layer * depth_steps + depth] /= float(max);
						
						vertex_buffer[first_vertex + layer * depth_steps + depth] = glm::rotate(glm::vec2(0, 0.21) + vertex_buffer[first_vertex + layer * depth_steps + depth], -(float(depth) / depth_steps) * glm::radians<float>(360));
						vertex_buffer[first_vertex + layer * depth_steps + depth].x *= 0.4 * horizontal_step;
						vertex_buffer[first_vertex + layer * depth_steps + depth].y *= 0.4 * vertical_step;
						vertex_buffer[first_vertex + layer * depth_steps + depth] += central_vertex; 
					}
				}
			}
		}
		
		vertices->write_from(sizeof(glm::vec2) * vertices_per_glyph * glyph_count, vertex_buffer);

		for (unsigned int y = 0; y < vertical_steps; y++)
		{
			for (unsigned int x = 0; x < horizontal_steps; x++)
			{
				tile_histogram& current_tile =  source[y * horizontal_steps + x];
				piechart& current_chart = piechart_buffer[y * horizontal_steps + x];

				unsigned int event_count = 0;

				for(unsigned int t = 0; t < event_types; t++)
				{
					current_chart.distribution[t] = 0;

					for(unsigned int d = 0; d < depth_steps; d++)
					{
						current_chart.distribution[t] += current_tile.histogram[t][d];
					}

					event_count += current_chart.distribution[t];
				}

				for (unsigned int t = 0; t < event_types; t++)
				{
					if(event_count > 0) current_chart.distribution[t] /= event_count;
				}
			}
		}

		piecharts->write_from(glyph_count * sizeof(piechart), piechart_buffer);

		if(*mode == time_mode::precision)
		{
			glm::uint m = 0;
			times->write_from(glyph_count * sizeof(precision_time), *time);
			times_mode->write_from(sizeof(glm::uint), &m);
		}
		else
		{
			glm::uint m = 1;
			times->write_from(glyph_count * sizeof(polynom_time), *time);
			times_mode->write_from(sizeof(glm::uint), &m);
		}
	}

	void glyph_renderer::attach_events(mmse_file& source)
	{
		mmse_file::event* event_buffer = (mmse_file::event*)calloc(source.file_header.event_count, sizeof(mmse_file::event));
		source.read_events(event_buffer);
		
		#pragma pack(push, 1)
		struct event_particle
		{
			glm::vec4 geometry;
			glm::uvec4 color;
		};
		#pragma pack(pop)

		event_particle* particle_buffer = (event_particle*)calloc(source.file_header.event_count, sizeof(event_particle));

		for (int i = 0; i < source.file_header.event_count; i++)
		{
			particle_buffer[i].geometry = glm::vec4(event_buffer[i].position.y, event_buffer[i].position.x, event_buffer[i].position.z, 0.f);
			if (event_buffer[i].type == 0) particle_buffer[i].color = glm::uvec4(114, 88, 147, 255);
			if (event_buffer[i].type == 1) particle_buffer[i].color = glm::uvec4(183, 88, 131, 255);
			if (event_buffer[i].type == 2) particle_buffer[i].color = glm::uvec4(247, 109, 130, 255);
			if (event_buffer[i].type == 3) particle_buffer[i].color = glm::uvec4(255, 158, 128, 255);
		}

		events->write_from(sizeof(event_particle) * source.file_header.event_count, particle_buffer);

		free(event_buffer);
		free(particle_buffer);
	}

	bool init = true;

	void glyph_renderer::camera_update(camera& camera)
	{
		bool camera_update = false;
		bool timeline_update = false;

		glfwPollEvents();

		if (glfwWindowShouldClose(window) == true)
		{
			glfwDestroyWindow(window);
			exit(EXIT_SUCCESS);
		}

		if(init)
		{
			init = false;
			camera_update = true;
			timeline_update = true;
			glfwSetWindowSize(window, camera.resolution.x, camera.resolution.y);
			camera.projection = glm::perspective(glm::radians(45.f), float(camera.resolution.x) / camera.resolution.y, 0.1f, 10000.f);
		}

		int width, height;
		glfwGetWindowSize(window, &width, &height);

		if (width != camera.resolution.x || height != camera.resolution.y)
		{
			camera.resolution = glm::uvec2(width, height);
			camera.projection = glm::perspective(glm::radians(45.f), float(camera.resolution.x) / camera.resolution.y, 0.1f, 10000.f);
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		{
			camera.position += 1.5f * camera.direction;
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			camera.position -= 1.5f * camera.direction;
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			camera.position -= 0.5f * glm::cross(camera.direction, glm::vec3(0, 1, 0));
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		{
			camera.position += 0.5f * glm::cross(camera.direction, glm::vec3(0, 1, 0));
			camera_update = true;
		}

		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS && X_pressed == false)
		{
			X_pressed = true;

			if (*mode == time_mode::precision)
			{
				*mode = time_mode::polynom;
				std::cout << "polynom mode" << std::endl;
			}
			else
			{
				*mode = time_mode::precision;
				std::cout << "precision mode" << std::endl;
			}
		}

		if (glfwGetKey(window, GLFW_KEY_X) != GLFW_PRESS && X_pressed == true)
		{
			X_pressed = false;
		}
		
		glm::dvec2 new_position;
		glfwGetCursorPos(window, &new_position.x, &new_position.y);

		// find out if on timeline

		glm::vec2 timeline_min(-0.38f, -0.97f);
		glm::vec2 timeline_max(0.38f, -0.87f);

		glm::dvec2 new_position_ndc(new_position.x / camera.resolution.x * 2.f - 1.f, -1.f * new_position.y / camera.resolution.y * 2.f + 1.f);

		if (new_position_ndc.x >= timeline_min.x && new_position_ndc.x <= timeline_max.x && new_position_ndc.y <= timeline_max.y && new_position_ndc.y >= timeline_min.y && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
		{
			time_step = 150 * (new_position_ndc.x + 0.38f) / 0.76f;
			if (time_step > 149) time_step = 149;
			timeline_update = true;
		}
		else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
		{
			camera.direction = glm::rotateX(camera.direction, 0.0005f * float(mouse.y - new_position.y));
			camera.direction = glm::rotateY(camera.direction, 0.0005f * float(new_position.x - mouse.x));
			camera_update = true;
		}

		mouse.x = new_position.x;
		mouse.y = new_position.y;

		if (camera_update)
		{
			camera.view = glm::lookAt(camera.position, camera.position + camera.direction, glm::vec3(0, 1, 0));
			matrices->write_from(sizeof(glm::mat4) * 2 + sizeof(glm::uvec2), &camera.view);
		}

		if(timeline_update)
		{
			timestep->write_from(sizeof(glm::uint), &time_step);
			std::cout << "new timestep: " << time_step << std::endl;
		}

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		{
			if (!R_pressed)
			{
				raster_active = !raster_active;
				R_pressed = true;
			}
			else R_pressed = false;
		}

		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		{
			if (!E_pressed)
			{
				events_active = !events_active;
				E_pressed = true;
			}
			else E_pressed = false;
		}

		if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS)
		{
			if (!G_pressed)
			{
				glyphs_active = !glyphs_active;
				G_pressed = true;
			}
			else G_pressed = false;
		}

		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
		{
			if(!T_pressed)
			{
				timeline_active = !timeline_active;
				T_pressed = true;
			}
			else T_pressed = false;
		}
	}

	void glyph_renderer::draw_glyphs()
	{	
		if (!glyphs_active) return;

		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ZERO);

		glBindVertexArray(vao);
		glyph_shader->use();
		vertices->bind(0);
		indices->bind(1);
		times->bind(2);
		times_mode->bind(3);
		timestep->bind(4);

		glDrawArrays(GL_TRIANGLES, 0, face_count * 3);
		
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		glEnable(GL_POINT_SPRITE);

		piechart_shader->use();
		vertices->bind(0);
		piecharts->bind(1);

		glDrawArrays(GL_POINTS, 0, horizontal_steps * vertical_steps);

		glDisable(GL_POINT_SPRITE);
		glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);

		
		glDisable(GL_BLEND);
	}

	void glyph_renderer::draw_events()
	{
		if (!events_active) return;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
		glEnable(GL_POINT_SPRITE);

		glBindVertexArray(vao);
		event_shader->use();
		matrices->bind(0);
		events->bind(1);

		glDrawArrays(GL_POINTS, 0, 16749);

		glDisable(GL_POINT_SPRITE);
		glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);
		glDisable(GL_BLEND);
	}

	void glyph_renderer::draw_raster()
	{
		if (!raster_active) return;

		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		glBindVertexArray(vao);
		raster_shader->use();
		raster->bind(0);
		matrices->bind(1);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glDisable(GL_BLEND);
	}

	void glyph_renderer::draw_timeline()
	{
		if (!timeline_active) return;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindVertexArray(vao);
		timeline_shader->use();
		matrices->bind(0);
		timestep->bind(1);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		glDisable(GL_BLEND);
	}

	void glyph_renderer::clear_buffer()
	{
		glClearColor(1.0, 1.0, 1.0, 0);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void glyph_renderer::swap_buffer()
	{
		glfwSwapBuffers(window);
	}
}