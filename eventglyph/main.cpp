#include "mmse_file.hpp"
#include "event_processor.hpp"
#include "glyph_renderer.hpp"
#include "../libraries/glm/gtc/matrix_transform.hpp"

int main()
{
	eventglyph::mmse_file f;
	f.attach_source("C:/Users/Stacknit/Desktop/glyph/exp2mill_events_5r-10_ms2-40_bd2.mmse");

	eventglyph::camera c;
	c.direction = glm::vec3(0, 0, 1);
	c.position = glm::vec3(700, 60, -300);
	c.resolution = glm::uvec2(1500, 500);

	eventglyph::event_processor p;
	p.attach_source(f);

	eventglyph::glyph_renderer r;
	r.attach_events(f);
	r.attach_source(p);

	while(true)
	{
		r.clear_buffer();
		r.camera_update(c);
		p.project_events(c);
		r.update_glyphs();
		r.draw_raster();
		r.draw_events();
		r.draw_glyphs();
		r.draw_timeline();
		r.swap_buffer();
	}

	return 0;
}