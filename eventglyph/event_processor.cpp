#include "event_processor.hpp"
#include <iostream>

namespace eventglyph
{
	event_processor::event_processor()
	{
		grid = (tile_histogram*)calloc(horizontal_steps * vertical_steps, sizeof(tile_histogram));
		tiles_range = (range*)calloc(horizontal_steps * vertical_steps, sizeof(range));
		mode = time_mode::precision;
		tiles_precision_time = (precision_time*)calloc(horizontal_steps * vertical_steps, sizeof(precision_time));
		tiles_polynom_time = (polynom_time*)calloc(horizontal_steps * vertical_steps, sizeof(polynom_time));
	}

	event_processor::~event_processor()
	{
		free(grid);
	}

	void event_processor::attach_source(mmse_file& source)
	{
		this->source = &source;

		events = (mmse_file::event*)calloc(source.file_header.event_count, sizeof(mmse_file::event));
		source.read_events(events);

		events_tile = (int*)calloc(source.file_header.event_count, sizeof(int));
		events_projection = (glm::vec4*)calloc(source.file_header.event_count, sizeof(glm::vec4));
	}

	void event_processor::project_events(camera& camera)
	{
		memset(grid, 0, horizontal_steps * vertical_steps * sizeof(tile_histogram));
		memset(events_tile, 0, source->file_header.event_count * sizeof(int));
		memset(events_projection, 0, source->file_header.event_count * sizeof(glm::vec4));
		memset(tiles_precision_time, 0, horizontal_steps * vertical_steps * sizeof(precision_time));
		memset(tiles_polynom_time, 0, horizontal_steps * vertical_steps * sizeof(polynom_time));

		for (unsigned int i = 0; i < horizontal_steps * vertical_steps; i++)
		{
			tiles_range[i].maximum = -100000.f;
			tiles_range[i].minimum = 100000.f;
		}

		float horizontal_step = 1.f / horizontal_steps;
		float vertical_step = 1.f / vertical_steps;

		for (unsigned int i = 0; i < source->file_header.event_count; i++)
		{
			mmse_file::event& event = events[i];

			glm::vec4 projected_position = camera.projection * camera.view * glm::vec4(event.position.y, event.position.x, event.position.z, 1.f);
			glm::vec4 normalized_coordinates = projected_position / projected_position.w;

			if (normalized_coordinates.x > 1.f || normalized_coordinates.x < -1.f || normalized_coordinates.y > 1.f || normalized_coordinates.y < -1.f)
			{
				events_tile[i] = -1;
				continue;
			}

			float x = normalized_coordinates.x * 0.5f + 0.5f;
			float y = normalized_coordinates.y * 0.5f + 0.5f;

			unsigned int horizontal_tile = unsigned int(x / horizontal_step);
			unsigned int vertical_tile = unsigned int(y / vertical_step);

			events_tile[i] = vertical_tile * horizontal_steps + horizontal_tile;
			events_projection[i] = projected_position;

			if (projected_position.z < tiles_range[events_tile[i]].minimum) tiles_range[events_tile[i]].minimum = projected_position.z;
			if (projected_position.z > tiles_range[events_tile[i]].maximum) tiles_range[events_tile[i]].maximum = projected_position.z;
		}

		for (unsigned int i = 0; i < source->file_header.event_count; i++)
		{
			if (events_tile[i] == -1) continue;

			mmse_file::event& event = events[i];

			tile_histogram& tile = grid[events_tile[i]];
			range& range = tiles_range[events_tile[i]];
			glm::vec4& projected_position = events_projection[i];

			float depth_step = (range.maximum - range.minimum) / depth_steps;
			unsigned int event_depth = (projected_position.z - range.minimum) / depth_step;

			tile.histogram[event.type][event_depth]++;

			tiles_precision_time[events_tile[i]].steps[event.type * depth_steps * time_steps + event_depth * time_steps + (unsigned int)event.time]++;
		}

		for(unsigned int t = 0; t < horizontal_steps * vertical_steps; t++)
		{
			for (unsigned int type = 0; type < event_types; type++)
			{
				for (unsigned int depth = 0; depth < depth_steps; depth++)
				{
					unsigned int accumulator = 0;
					
					for (unsigned int step = 0; step < time_steps; step++)
					{
						accumulator += tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + step];
						tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + step] = accumulator;
					}

					if (accumulator > 0)
					{
						for (unsigned int step = 0; step < time_steps; step++)
						{
							tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + step] /= accumulator;
						}
					}

					tiles_polynom_time[t].polynom[type * depth_steps + depth].x = tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + 0];

					tiles_polynom_time[t].polynom[type * depth_steps + depth].y = tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + 44];

					tiles_polynom_time[t].polynom[type * depth_steps + depth].z = tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + 104];

					tiles_polynom_time[t].polynom[type * depth_steps + depth].a = tiles_precision_time[t].steps[type * depth_steps * time_steps + depth * time_steps + 149];
				}
			}
		}

		if (mode == time_mode::precision) time = (float*)tiles_precision_time;
		else time = (float*)tiles_polynom_time;
	}
}