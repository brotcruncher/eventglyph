#version 430 core

layout (std430, binding = 0) buffer vertex_buffer
{
	vec2 vertices[];
};

layout (std430, binding = 1) buffer index_buffer
{
	uint indices[];
};

const uint depth_steps = 8;
const uint event_types = 4;
const uint faces_per_layer = (depth_steps - 1) * 2;
const uint vertices_per_layer = 3 * faces_per_layer;
const uint vertices_per_glyph = depth_steps + depth_steps * event_types;

out flat uint event_type; 
out vec2 texture_coordinate;
out flat uint quad;

const vec2 texture_coordinates[6] = vec2[](vec2(0, 0), vec2(0, 1), vec2(1, 0), vec2(1, 0), vec2(0, 1), vec2(1, 1));

void main()
{
	uint vertex = gl_VertexID % (vertices_per_layer * event_types);
	uint glyph = gl_VertexID / (vertices_per_layer * event_types);
	event_type = uint(vertex / vertices_per_layer);
	uint depth_step = (vertex % vertices_per_layer) / 6;
	quad = glyph * event_types * depth_steps + event_type * depth_steps + depth_step;

	uint in_quad_vertex = vertex % 6;
	texture_coordinate = texture_coordinates[in_quad_vertex];

	gl_Position = vec4(vertices[glyph * vertices_per_glyph + indices[vertex]], 0.f, 1.f);
}