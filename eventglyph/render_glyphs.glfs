#version 430 core

in flat uint event_type;
in vec2 texture_coordinate;
in flat uint quad;

layout (std430, binding = 2) buffer times_buffer
{
	float times[];
};

layout (std430, binding = 3) buffer times_mode_buffer
{
	uint mode;
};

layout (std430, binding = 4) buffer time_step_buffer
{
	uint timestep;
};

out vec4 color;

const vec4 t0_base = vec4(255, 158, 128, 255);
const vec4 t0_bright = vec4(255, 215, 202, 255);
const vec4 t0_dark = vec4(179, 111, 90, 255);

const vec4 t1_base = vec4(247, 109, 130, 255);
const vec4 t1_bright = vec4(249, 144, 159, 255);
const vec4 t1_dark = vec4(184, 80, 96, 255);

const vec4 t2_base = vec4(183, 88, 131, 255);
const vec4 t2_bright = vec4(182, 128, 152, 255);
const vec4 t2_dark = vec4(121, 58, 87, 255);

const vec4 t3_base = vec4(114, 88, 147, 255);
const vec4 t3_bright = vec4(147, 130, 169, 255);
const vec4 t3_dark = vec4(79, 61, 102, 255);

void main()
{
	float timestep_value; // 0 bis 1

	if(mode == 0)
	{
		timestep_value = times[quad * 150 + timestep];
	}
	else
	{
		float v0 = times[quad * 4];
		float v1 = times[quad * 4 + 1];
		float v2 = times[quad * 4 + 2];
		float v3 = times[quad * 4 + 3];

		if(timestep < 44)
		{
			float scaling = timestep / 43.f;
			timestep_value = v0 * (1 - scaling) + scaling * v1;
		}
		else if(timestep < 104)
		{
			float scaling = (timestep - 44.f) / 60.f;
			timestep_value = v1 * (1 - scaling) + scaling * v2;
		}
		else if(timestep < 149)
		{
			float scaling = (timestep - 104.f) / 45.f;
			timestep_value = v2 * (1 - scaling) + scaling * v3;
		}
	}

	/*
	if((timestep_value > texture_coordinate.y && timestep_value - texture_coordinate.y < 0.1f) || (timestep_value < texture_coordinate.y && texture_coordinate.y - timestep_value < 0.1f))
	{
		color = vec4(0, 0.8, 0.5, 1);
		return;
	}
	*/

	if(texture_coordinate.y < timestep_value)
	{
		// brighter = before timestep
		float scaling = texture_coordinate.y / timestep_value;

		if(event_type == 0) color = (t0_base * scaling + t0_bright * (1.f - scaling)) / 255;
		else if(event_type == 1) color = (t1_base * scaling + t1_bright * (1.f - scaling)) / 255;
		else if(event_type == 2) color = (t2_base * scaling + t2_bright * (1.f - scaling)) / 255;
		else color = (t3_base * scaling + t3_bright * (1.f - scaling)) / 255;
	}
	else
	{
		// darker = after timestep
		float range = 1.f - timestep_value;
		float scaling = (texture_coordinate.y - timestep_value) / range;

		if(event_type == 0) color = (t0_base * (1.f - scaling) + t0_dark * scaling) / 255;
		else if(event_type == 1) color = (t1_base * (1.f - scaling) + t1_dark * scaling) / 255;
		else if(event_type == 2) color = (t2_base * (1.f - scaling) + t2_dark * scaling) / 255;
		else color = (t3_base * (1.f - scaling) + t3_dark * scaling) / 255;
	}
}