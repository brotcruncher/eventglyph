#include "shader_toolkit.hpp"
#include <iostream>
#include <fstream>
#include <sstream>

namespace eventglyph
{
	shader_program::shader_program()
	{
		id = glCreateProgram();
	}

	void shader_program::attach(GLenum type, string path)
	{
		if (type != GL_VERTEX_SHADER && type != GL_TESS_CONTROL_SHADER &&
			type != GL_TESS_EVALUATION_SHADER && type != GL_GEOMETRY_SHADER &&
			type != GL_FRAGMENT_SHADER && type != GL_COMPUTE_SHADER)
		{
			std::cout << "[shader_program] shader type unknown." << std::endl;
			exit(EXIT_FAILURE);
		}

		GLuint shader = glCreateShader(type);

		ifstream file;
		file.open(path, ios::in);

		if (!file.good()) std::cout << "[shader_program] file opening error." << std::endl;

		std::stringstream stream;
		stream << file.rdbuf();
		file.close();
		std::string string = stream.str();
		const char *source = string.c_str();
		glShaderSource(shader, 1, &source, NULL);

		glCompileShader(shader);

		GLint status;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE)
		{
			GLint length;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);

			GLchar *log = new GLchar[length];
			glGetShaderInfoLog(shader, length, &length, log);

			std::string output;

			switch (type)
			{
				case GL_VERTEX_SHADER: {  output = "Vertex"; break; }
				case GL_TESS_CONTROL_SHADER: { output = "Tessellation Control"; break; }
				case GL_TESS_EVALUATION_SHADER: { output = "Tessellation Evaluation"; break; }
				case GL_GEOMETRY_SHADER: { output = "Geometry"; break; }
				case GL_FRAGMENT_SHADER: { output = "Fragment"; break; }
				case GL_COMPUTE_SHADER: { output = "Compute"; break; }
			}

			std::cout << output << " Shader compilation failed: " << log << std::endl;

			delete[] log;
		}

		glAttachShader(id, shader);
	}

	void shader_program::link()
	{
		glLinkProgram(id);

		GLint status;
		glGetProgramiv(id, GL_LINK_STATUS, &status);
		if (status == GL_FALSE)
		{
			GLint length;
			glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length);

			GLchar* log = new GLchar[length];
			glGetProgramInfoLog(id, length, &length, log);

			std::cout << "[shader_program] linking error: " << log << std::endl;

			delete[] log;
		}
	}

	void shader_program::use()
	{
		glUseProgram(id);
	}

	shader_storage::shader_storage()
	{
		glGenBuffers(1, &id);
	}

	void shader_storage::write_from(GLuint size, GLvoid* data)
	{
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
		glBufferData(GL_SHADER_STORAGE_BUFFER, size, data, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}

	void shader_storage::read_to(GLuint size, GLvoid *data)
	{
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
		glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, size, data);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
	}

	void shader_storage::bind(GLuint position)
	{
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, position, id);
	}
}